# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Frontend Mentor - QR code component solution](#frontend-mentor---qr-code-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
  - [Author](#author)

## Overview

### Screenshot

[![](https://i.imgur.com/VICDMpvm.jpg)](https://i.imgur.com/VICDMpv.png)
[![](https://i.imgur.com/gv5ahe5m.jpg)](https://i.imgur.com/gv5ahe5.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/frontendmentor3/frontendmentor-qr-code-component/-/tree/master)
- Live Site URL: [Gitlab pages](https://frontendmentor3.gitlab.io/frontendmentor-qr-code-component)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox

## Author

- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)
